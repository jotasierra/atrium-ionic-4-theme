import { Component } from '@angular/core';
import { Platform, NavController,ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';


import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { Pages } from './interfaces/pages';
import { Storage } from '@ionic/Storage';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  name =null;
  public appPages: Array<Pages>;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public navCtrl: NavController,
    private router: Router,
    public toastCtrl: ToastController,
    private storage: Storage
  ) {
    this.appPages = [
      {
        title: 'Home',
        url: '/home-results',
        direct: 'root',
        icon: 'home'
      },
      {
        title: 'About',
        url: '/about',
        direct: 'forward',
        icon: 'information-circle-outline'
      },

      {
        title: 'App Settings',
        url: '/settings',
        direct: 'forward',
        icon: 'cog'
      }
    ];

    this.initializeApp();
    
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    }).catch(() => {});

    this.storage.get('session_storage').then((res)=>{
      if(res == null){
        this.router.navigate(['/']);
        console.log("aqui");
      }else{
        console.log(res);
        this.name  = res.FirstName + ' ' +res.LastName;
        this.router.navigate(['/home-results']);
      }
    });

  }

  goToEditProgile() {
    this.navCtrl.navigateForward('edit-profile');
  }

  async logout(){
    this.storage.clear();
    this.router.navigate(['/']);
    const toast = await this.toastCtrl.create({
        message: 'logout succesful',
        duration: 3000
      });
    toast.present();
  }
}
